DROP DATABASE IF EXISTS e_commerce27;
CREATE DATABASE e_commerce27;
USE e_commerce27;

CREATE TABLE prodotto (
prodottoId INTEGER NOT NULL auto_increment primary KEY,
nome VARCHAR(250) NOT NULL,
codice VARCHAR(250)  UNIQUE,
prezzo float NOT NULL,
quantita INTEGER NOT NULL
);

insert into prodotto (nome, prezzo, quantita) values ('Tea - Lemon Green Tea', 13.7, 8);
insert into prodotto (nome, prezzo, quantita) values ('Cheese - Cheddar, Old White', 11.0, 83);
insert into prodotto (nome, prezzo, quantita) values ('Salmon - Canned', 8.1, 73);
insert into prodotto (nome, prezzo, quantita) values ('Cleaner - Lime Away', 14.5, 76);
insert into prodotto (nome, prezzo, quantita) values ('Beef Flat Iron Steak', 14.9, 100);
insert into prodotto (nome, prezzo, quantita) values ('Rice - Jasmine Sented', 13.5, 99);
insert into prodotto (nome, prezzo, quantita) values ('Pepper Squash', 14.9, 84);
insert into prodotto (nome, prezzo, quantita) values ('Brocolinni - Gaylan, Chinese', 15.6, 22);
insert into prodotto (nome, prezzo, quantita) values ('Chicken - Whole', 13.2, 10);
insert into prodotto (nome, prezzo, quantita) values ('Cafe Royale', 3.5, 92);
insert into prodotto (nome, prezzo, quantita) values ('Tea - Herbal - 6 Asst', 17.7, 59);
insert into prodotto (nome, prezzo, quantita) values ('Appetizer - Mini Egg Roll, Shrimp', 12.4, 68);
insert into prodotto (nome, prezzo, quantita) values ('Lamb - Racks, Frenched', 10.9, 9);
insert into prodotto (nome, prezzo, quantita) values ('Garlic Powder', 17.9, 87);
insert into prodotto (nome, prezzo, quantita) values ('Mustard - Individual Pkg', 17.1, 14);
insert into prodotto (nome, prezzo, quantita) values ('Mushroom - King Eryingii', 11.7, 87);
insert into prodotto (nome, prezzo, quantita) values ('Egg - Salad Premix', 3.2, 69);
insert into prodotto (nome, prezzo, quantita) values ('Fib N9 - Prague Powder', 3.6, 32);
insert into prodotto (nome, prezzo, quantita) values ('Mix - Cappucino Cocktail', 3.9, 47);
insert into prodotto (nome, prezzo, quantita) values ('Soup Campbells Beef With Veg', 6.4, 70);

CREATE TABLE cliente(
clienteId INTEGER NOT NULL auto_increment primary KEY,
username VARCHAR(250) NOT NULL UNIQUE,
psw VARCHAR(250) NOT NULL UNIQUE,
ruolo VARCHAR(3) 
);

insert into cliente (username, psw,ruolo) values ('lvanini0', 'Q2ktaDuwrUXy','ADM');
insert into cliente (username, psw,ruolo) values ('awayman1', 'tWtfT9ai','ADM');
insert into cliente (username, psw,ruolo) values ('tberrisford2', '9fREFN','ADM');
insert into cliente (username, psw,ruolo) values ('mpreator3', 'UgcSrXhKj','UTE');
insert into cliente (username, psw,ruolo) values ('csaw4', 'HZME30Kt7nCx','UTE');
insert into cliente (username, psw,ruolo) values ('mtoombs5', 'ZJfznttIDHo','UTE');
insert into cliente (username, psw,ruolo) values ('dlanphere6', 'akX0UaBnbWm','UTE');
insert into cliente (username, psw,ruolo) values ('cweblin7', 'xLi7ju','UTE');
insert into cliente (username, psw,ruolo) values ('rcrumpe8', 'tHEMykl','UTE');
insert into cliente (username, psw,ruolo) values ('aterrill9', 'PnQ9Q1tcxsXi','UTE');
insert into cliente (username, psw,ruolo) values ('lfetteplacea', 'TpRyvOeY','UTE');
insert into cliente (username, psw,ruolo) values ('lseagoodb', 'ccNqo4Ag','UTE');
insert into cliente (username, psw,ruolo) values ('wrowthornc', 'pQQjsQO1','UTE');
insert into cliente (username, psw,ruolo) values ('cbibbd', 'HczurorHt','UTE');
insert into cliente (username, psw,ruolo) values ('lgerbe', 'kmF0RXFGre','UTE');
insert into cliente (username, psw,ruolo) values ('cbolstridgef', 'Pm4lDR2tupU','UTE');
insert into cliente (username, psw,ruolo) values ('mthowlessg', 'WSHTpzmCuU76','UTE');
insert into cliente (username, psw,ruolo) values ('ekordah', 'avnwTR','UTE');
insert into cliente (username, psw,ruolo) values ('bwinmilli', 'ppd8wHJz','UTE');
insert into cliente (username, psw,ruolo) values ('fkellsj', 'lUdin7o','UTE');


select* from prodotto;
select* from cliente;