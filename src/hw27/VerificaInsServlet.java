package hw27;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hw27.classi.Gestore;
import hw27.classi.Prodotto;

/**
 * Servlet implementation class VerificaInsServlet
 */
@WebServlet("/VerificaInsServlet")
public class VerificaInsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VerificaInsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nome = request.getParameter("input_nome");
		Float prezzo = Float.parseFloat(request.getParameter("input_prezzo"));
		Float quantita = Float.parseFloat(request.getParameter("input_quantita"));
		Prodotto prod = checkProdotto(nome, prezzo, quantita);
		if(prod!=null) {
			Gestore gest = new Gestore();
			try {
				if(gest.insertProd(prod)!=null) {
					PrintWriter out = response.getWriter();
					out.println("<script> alert(\"Inserimento avvenuto con successo! \"); </script>");
				}
				else {
					PrintWriter out = response.getWriter();
					out.println("<script> alert(\"Errore nell'inserimento del prodotto! \"); </script>");		
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		else {
			PrintWriter out = response.getWriter();
			out.println("<script> alert(\"Errore nell'inserimento del prodotto! \"); </script>");		
		}
		

	}
	
	public Prodotto checkProdotto(String nome, Float prezzo, Float quantita) {
		if(!nome.isBlank() && prezzo!=null && quantita!=null) {
			if(quantita %1 == 0) {
				Prodotto prod = new Prodotto();
				prod.setNome(nome);
				prod.setPrezzo(prezzo);
				prod.setQuantita(quantita);
				return prod;
			}
		}
		
		return null;
	}

}
