package hw27;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hw27.classi.Gestore;

/**
 * Servlet implementation class EliminaServlet
 */
@WebServlet("/EliminaServlet")
public class EliminaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EliminaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] elenco_cod = request.getParameterValues("id");
		Gestore gest = new Gestore();
		boolean ok = true;
		if(elenco_cod!=null) {
			for (int i = 0; i < elenco_cod.length; i++) {
				
				try {
					ok = gest.deleteByCodice(elenco_cod[i]);
					
					if(!ok) {
						PrintWriter out = response.getWriter();
						out.println("<script> alert(\"Problemi nell'eliminazione dell'item con codice: "+elenco_cod[i]+" \"); </script>");
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(ok) {
				PrintWriter out = response.getWriter();
				out.println("<script> alert(\"Eliminazione avvenuta con successo! \"); </script>");
			}
		}
	}

}
