package hw27.classi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import hw27.connessione.*;

public class Gestore {
	private ArrayList<Prodotto> elenco_prod = new ArrayList<Prodotto>();
	private ArrayList<Cliente> elenco_cli = new ArrayList<Cliente>();
	

	
	
	public ArrayList<Prodotto> getElenco_prod() {
		return elenco_prod;
	}

	public void setElenco_prod(ArrayList<Prodotto> elenco_prod) {
		this.elenco_prod = elenco_prod;
	}

	public ArrayList<Cliente> getElenco_cli() {
		return elenco_cli;
	}

	public void setElenco_cli(ArrayList<Cliente> elenco_cli) {
		this.elenco_cli = elenco_cli;
	}

	public void findAll() throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "SELECT prodottoId,nome,prezzo,quantita FROM prodotto ";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Prodotto prod = new Prodotto();
			prod.setId(risultato.getInt(1));
			prod.setNome(risultato.getString(2));
			prod.setPrezzo(risultato.getFloat(3));
			prod.setQuantita(risultato.getInt(4));
			String codice = generaCodice(prod.getId(), prod.getNome());
			prod.setCodice(codice);		
			elenco_prod.add(prod);
		}
	}
	
	public String generaCodice(int id, String nome) throws SQLException {
		String codice = nome.substring(0, 3).toUpperCase()+id;
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "UPDATE prodotto SET codice = ? WHERE prodottoId = ?";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, codice);
		ps.setInt(2, id);
		ps.executeUpdate();	
		return codice;
	}
	
	public Prodotto updateByCodice(Prodotto prod) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "UPDATE prodotto SET nome = ? ,prezzo = ? ,quantita = ?  WHERE codice = ?";
		;
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, prod.getNome());
		ps.setFloat(2, prod.getPrezzo());
		ps.setFloat(3, prod.getQuantita());
		ps.setString(4, prod.getCodice());
		int risultato_update = ps.executeUpdate();
		if(risultato_update>0)
			return prod;
		else {
			return null;
		}
		
	}
	
	public boolean deleteByCodice(String codice) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "delete from prodotto where codice = ? ";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);
		ps.setString(1, codice);
		int risultato_update = ps.executeUpdate();
		if(risultato_update>0)
			return true;
		else {
			return false;
		}

	}
	
	public Cliente checkLog(String user, String psw) throws SQLException {
		Cliente cli = new Cliente();
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "SELECT username,psw,ruolo FROM cliente WHERE username=? && psw=? ";
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query);	
		ps.setString(1, user);
		ps.setString(2, psw);
		ResultSet risultato = ps.executeQuery();
		if(risultato.next()) {
			cli.setUser(risultato.getString(1));
			cli.setPsw(risultato.getString(2));
			cli.setRuolo(risultato.getString(3));
			
			return cli;
		}
		else
			return null;
	}
	
	public Prodotto insertProd(Prodotto prod) throws SQLException {
		Connection connessione = ConnessioneDB.getIstanza().getConnessione();
		String query = "insert into prodotto (nome,prezzo,quantita) values(?,?,?)" ;
		PreparedStatement ps = (PreparedStatement) connessione.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);	
		ps.setString(1, prod.getNome());
		ps.setFloat(2, prod.getPrezzo());
		ps.setFloat(3, prod.getQuantita());
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		int id = risultato.getInt(1);
		if(id>0) {
			prod.setId(id); 
			String codice = generaCodice(prod.getId(), prod.getNome());
			prod.setCodice(codice);		
			elenco_prod.add(prod);	
			return prod;
		}
		return null;	
	}
}
