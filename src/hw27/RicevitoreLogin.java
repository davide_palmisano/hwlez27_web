package hw27;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hw27.classi.Cliente;
import hw27.classi.Gestore;

/**
 * Servlet implementation class Ricevitore
 */
@WebServlet("/Ricevitore")
public class RicevitoreLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RicevitoreLogin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String user = request.getParameter("input_user");
		String psw = request.getParameter("input_psw");
		
		Gestore gest = new Gestore();
		try {
			if(gest.checkLog(user, psw) == null) {
				PrintWriter out = response.getWriter();
				out.println("<script> alert(\"Errore nell'inserimento dell'Username o della Password!\"); </script>");
			}
			else {
				Cliente cli = gest.checkLog(user, psw);
				if(cli.getRuolo().equals("ADM")) {
					// TODO MANDA A PAGINA ADMIN
					PrintWriter out = response.getWriter();
					out.println("<script> alert(\"Benvenuto Admin : "+cli.getUser()+" \"); </script>");
					response.sendRedirect("Admin.html");
					
				}
				else if(cli.getRuolo().equals("UTE")) {
					// TODO Manda a pagina utente semplice
					PrintWriter out = response.getWriter();
					out.println("<script> alert(\"Benvenuto User : "+cli.getUser()+" \"); </script>");
				}
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	

}
