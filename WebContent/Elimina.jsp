<%@page import="hw27.classi.Prodotto"%>
<%@page import="hw27.classi.Gestore"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<title>Elenco Prodotti</title>
</head>
<body>
	<form action="EliminaServlet" method="post">
		<div class="container">
			<div class="row">
				<div class="col">
					<table class="table table-striped table-hover">
						<thead>
							<tr> 
								<th> Nome </th>
								<th> Codice </th>
								<th> Prezzo </th>
								<th> Quantita </th>
								<th> <i class="fas fa-trash"></i> </th>
							</tr>
						 </thead>
						<tbody>
							<%
								Gestore gestore = new Gestore();
								gestore.findAll();
								String riga = "";
								for(Prodotto prod : gestore.getElenco_prod()){
									riga += "<tr>";
									riga += "<td>"+prod.getNome()+"</td>";
									riga += "<td>"+prod.getCodice()+"</td>";
									riga += "<td>"+prod.getPrezzo()+" $"+"</td>";
									riga += "<td>"+prod.getQuantita()+"</td>";
									riga += "<td><input type=\"checkbox\" name=\"id\" value="+prod.getCodice()+"></td>";
									riga += "</tr>";								
								}
								
								out.print(riga);
							%>
							
						</tbody>
					</table>
					<button type="submit" class="btn btn btn-success btn-block" style="margin-top: 10px;" >ELIMINA ITEM SELEZIONATI</button>				
				</div>
			</div>
		</div>
	</form>
	
	<div class="container">
		<form action="Admin.html">
			<button style="margin-top: 10px;" type="submit" class="btn btn-outline-primary btn-block">TORNA ALLA PAGINA ADMIN</button>
		</form>
	</div>






    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>  
</body>
</html>